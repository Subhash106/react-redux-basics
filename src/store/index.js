import { configureStore } from '@reduxjs/toolkit';

import counterReducer from './counter-reducer';
import authenticationReducer from './auth-reducer';

const store = configureStore({
    reducer: {
        counter: counterReducer,
        auth: authenticationReducer
    }
});

export default store;