
import { createSlice } from "@reduxjs/toolkit";

const defaultAuthenticationState = { isAuthenticated: false };

const authenticationSlice = createSlice({
    name: 'authentication',
    initialState: defaultAuthenticationState,
    reducers: {
        login (state) {
            state.isAuthenticated = true;
        },
        logout (state) {
            state.isAuthenticated = false;
        }
    }
});

export const authActions = authenticationSlice.actions;

export default authenticationSlice.reducer;
